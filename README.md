# Modelling learning Russian knowledge space

Modelling structure <br/>
 -- Modelling concepts with CEFR <br/>
 ---- Student model with BKT <br/>
 ------- Domain model with Grow-Shrink Markov blanket <br/>
 ------- Domain model with Greedy score-based algorithm <br/>
 ------- Domain model with Theory-driven algorithm <br/>
 ---- Student model with Threshold estimator <br/>
 ------- Domain model with Grow-Shrink Markov blanket <br/>
 ------- Domain model with Greedy score-based algorithm <br/>
 ------- Domain model with Theory-driven algorithm <br/>
<br/>
 -- Modelling concepts without CEFR <br/>
 ---- Student model with BKT <br/>
 ------- Domain model with Grow-Shrink Markov blanket <br/>
 ------- Domain model with Greedy score-based algorithm <br/>
 ------- Domain model with Theory-driven algorithm <br/>
 ---- Student model with Threshold estimator <br/>
 ------- Domain model with Grow-Shrink Markov blanket <br/>
 ------- Domain model with Greedy score-based algorithm <br/>
 ------- Domain model with Theory-driven algorithm <br/>
 